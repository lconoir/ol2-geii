# OL2-GEII

Dépôt de TP du module OL2 de la formation GEII

## Accès à l'ensemble
Dans le [répertoire TP](TP)

## Accès par TP
|Enoncé|
|:-:|
|[TP 1](TP/OL2 TP 1 2021.pdf)|
|[TP 2](TP/OL2 TP 2 2021.pdf)|
|[TP 3](TP/OL2 TP 3 2021.pdf)|
|[TP 4](TP/OL2 TP 4 2021.pdf)|
|[TP 5](TP/OL2 TP 5 2021.pdf)|
|[TP 6](TP/OL2 TP 6 2021.pdf)|

Les corrigés ne sont pas visualisables sur ce dépôt.  
Il faut les ouvrir avec Maxima.
